function initMap() {
        // var map = new google.maps.Map(document.getElementById('map'), {
        //   zoom: 13,
        //   center: {lat: 43.5164259, lng: 39.8174274},
        //   disableDefaultUI: true
        // });

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: {lat: 43.5164259, lng: 39.888274},
          disableDefaultUI: true
        });

        // if ($(window).width() < 1023) {

        //     var map = new google.maps.Map(document.getElementById('map'), {
        //       zoom: 13,
        //       center: {lat: 43.5164259, lng: 39.888274},
        //       disableDefaultUI: true
        //     });
        // }

        var marker = new google.maps.Marker({
              position: {lat: 43.4865114, lng: 39.8910159},
              icon: 'img/map-marker.png',
              map: map
            });


        var styles = 

        [
          {
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#f5f5f5"
              }
            ]
          },
          {
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#f5f5f5"
              }
            ]
          },
          {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#bdbdbd"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#eeeeee"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#e5e5e5"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#ffffff"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#dadada"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          },
          {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#e5e5e5"
              }
            ]
          },
          {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#eeeeee"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#ededed"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#ededed"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          }
        ]

        map.setOptions({styles: styles});

      }


$(function () {

    function YMGoal(goal_id) {
        ym(65281939, 'reachGoal', goal_id);
        return true;
    }

  if ($(window).width() > 1023) {

    $('.parallax-wrap').parallax({
      calibrateX: true,
      calibrateY: false,
      limitX: false,
      limitY: false,
      scalarX: 2,
      scalarY: 10,
      frictionX: 0.2,
      frictionY: 0.2
    });

  }

  if($('.structure__slider').length > 0) {
        $('.structure__slider').slick({
            slidesToShow: 1,
            infinite: true,
            dots: true,
            speed: 700,
            slidesToScroll: 1,
            fade: true,
            cssEase: 'linear',

            customPaging : function(slider, i) {
              var title = $(slider.$slides[i]).data('title');
              return '<a class="pager__item"> '+title+' </a>';
            },
        });
      $('.structure__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
          $('.structure-slider-parallax-items').find('.parallax-item').removeClass('visible').filter('[data-slide="'+nextSlide+'"]').addClass('visible');
      })
    }



  $(".tabs-js").on("click", "span:not(.active)", function(e) {
          $(this)
          .addClass("active")
          .siblings()
          .removeClass("active")
          .parents(".tabs-info-js")
          .find(".main-form__tabs-content")
          .children()
          .hide()
          .eq($(this).index())
          .fadeIn(300);

  });  

  $(".info-panel__close").on("click", function() {
    $(".info-panel").hide();
  });  

  $('.form-tel-name').each(function(i, item) {
       var $form = $(item);

       $form.validate({
         rules: {
             tel: {
                 required: true
             },
             name: {
                 required: true
             },
             checker: {
                required: true
             }

         },
         messages: {
             tel: {
                 required: 'Укажите ваш телефон'
             },
             name: {
                 required: 'Укажите ваше имя'
             },
             checker: {
                required: 'Это поле обязательно'
             }
         },
         submitHandler: function(form) {
             var $form = $(form);
             $.ajax({
                 type: "POST",
                 url: "tel-name.php",
                 data: $form.serialize()
             }).done(function(e) {
                 $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                 $form.trigger('reset');

                 /* берем из формы id цели яндекс метрики */
                 let $ym_input = $form.find('input[name="ym_id"]');
                 if ($ym_input.length > 0) {
                     let ym_id = $ym_input.val();
                     /* отправляем цель в я.метрику */
                     YMGoal(ym_id);
                 }

                 // window.location = "thanks-page.html"
             }).fail(function (e) {
                 $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                 $form.trigger('reset');
                 setTimeout(function () {
                     $form.find('.dispatch-message').slideUp();
                 }, 5000);
             })
         }
       });

     });




    function quiz() {
        let currentStep = 1,
            fadeSpeed = 500,
            $btnNext = $('.jsQuizNext'),
            $step1 = $('.quiz-step.step-1'),
            $step2 = $('.quiz-step.step-2'),
            $step3 = $('.quiz-step.step-3'),
            $step4 = $('.quiz-step.step-4'),
            $step5 = $('.quiz-step.step-5'),
            $step6 = $('.quiz-step.step-6'),
            $buttonNextWrap = $('.quiz-button-next'),
            $buttonFinalWrap = $('.quiz-button-final'),
            $currStepNum = $('.jsQuizCurrStep'),
            $descriptionMain = $('.quiz-description-main'),
            $descriptionFinal = $('.quiz-description-final');

        $btnNext.on('click', function (e) {
            e.preventDefault();
            if (currentStep === 1) {
                $step1.hide();
                $step2.fadeIn(fadeSpeed);

                currentStep = 2;
            } else if (currentStep === 2) {
                $step2.hide();
                $step3.fadeIn(fadeSpeed);

                currentStep = 3;
            } else if (currentStep === 3) {

                $step3.hide();
                $step4.fadeIn(fadeSpeed);

                currentStep = 4;

            } else if (currentStep === 4) {

                $step4.hide();
                $step5.fadeIn(fadeSpeed);

                currentStep = 5;

            } else if (currentStep === 5) {

                $step5.hide();
                $step6.fadeIn(fadeSpeed);

                $buttonNextWrap.hide();
                $buttonFinalWrap.fadeIn(fadeSpeed);

                $descriptionMain.hide();
                $descriptionFinal.fadeIn(fadeSpeed);

                currentStep = 5;
            }

            setTimeout(function () {
                $currStepNum.html(currentStep);
            }, fadeSpeed/2);
        });

        $('.form-quiz').validate({
            rules: {
                tel: {
                    required: function(element) {
                        return $('.form-quiz input[name="tel"]').is(':visible')
                    }
                },
                email: {
                    required: function(element) {
                        return $('.form-quiz input[name="email"]').is(':visible')
                    }
                },
            },
            messages: {
                tel: {
                    required: 'Укажите ваш телефон',
                    tel: 'Укажите существующий телефон'
                },
                email: {
                    required: 'Укажите ваш email',
                    email: 'Укажите существующий email'
                },
            },
            submitHandler: function(form) {
                var $form = $(form);
                $.ajax({
                    type: "POST",
                    url: "email-quiz.php",
                    data: $form.serialize()
                }).done(function(e) {
                    $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                    $form.trigger('reset');

                    /* берем из формы id цели яндекс метрики */
                    let $ym_input = $form.find('input[name="ym_id"]');
                    if ($ym_input.length > 0) {
                        let ym_id = $ym_input.val();
                        /* отправляем цель в я.метрику */
                        YMGoal(ym_id);
                    }

                }).fail(function (e) {
                    $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                    $form.trigger('reset');
                    setTimeout(function () {
                        $form.find('.dispatch-message').slideUp();
                    }, 5000);
                })
            }
        });

    }
    quiz();
  
});